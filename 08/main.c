#include <stdio.h>
#include <errno.h>
#include <libudev.h>

int main ()
{
	struct udev *udev;

	udev = udev_new ();

	if (!udev)
	{
		perror ("udev_new failure");
		return errno;
	}

	struct udev_enumerate *udev_enumerate;

	udev_enumerate = udev_enumerate_new (udev);

	if (!udev_enumerate)
	{
		perror ("udev_enumerate_new failure");
		udev_unref (udev);
		return errno;
	}

	udev_enumerate_add_match_subsystem (udev_enumerate, "usb");

	if (udev_enumerate_scan_devices (udev_enumerate) < 0)
	{
		perror ("udev_enumerate_scan_devices failure");
		udev_enumerate_unref (udev_enumerate);
		udev_unref (udev);
		return errno;
	}

	struct udev_list_entry *udev_list_entries;

	udev_list_entries = udev_enumerate_get_list_entry (udev_enumerate);

	if (!udev_list_entries)
	{
		perror ("udev_enumerate_get_list_entry failure");
		udev_enumerate_unref (udev_enumerate);
		udev_unref (udev);
		return errno;
	}

	struct udev_list_entry *udev_list_entry;

	udev_list_entry_foreach (udev_list_entry, udev_list_entries)
	{
		const char *name;

		name = udev_list_entry_get_name (udev_list_entry);

		struct udev_device *udev_device;
		
		udev_device = udev_device_new_from_syspath (udev, name);

		if (!udev_device)
		{
			perror ("udev_device_new_from_syspath failure");
			udev_enumerate_unref (udev_enumerate);
			udev_unref (udev);
			return errno;
		}

		const char *syspath, *sysname, *sysnum, *devpath, *devnode,
		      *devtype, *subsystem, *driver, *idvendor, *idproduct,
		      *manufacturer, *product, *serial;

		syspath = udev_device_get_syspath (udev_device);
		sysname = udev_device_get_sysname (udev_device);
		sysnum = udev_device_get_sysnum (udev_device);
		devpath = udev_device_get_devpath (udev_device);
		devnode = udev_device_get_devnode (udev_device);
		devtype = udev_device_get_devtype (udev_device);
		subsystem = udev_device_get_subsystem (udev_device);
		driver = udev_device_get_driver (udev_device);

		struct udev_device *parent = udev_device_get_parent_with_subsystem_devtype (
				udev_device, "usb", "usb_device");

		if (!parent)
		{
			udev_device_unref (udev_device);
			continue;
		}

		idvendor = udev_device_get_sysattr_value (parent, "idVendor");
		idproduct = udev_device_get_sysattr_value (parent, "idProduct");
		manufacturer = udev_device_get_sysattr_value (parent, "manufacturer");
		product = udev_device_get_sysattr_value (parent, "product");
		serial = udev_device_get_sysattr_value (parent, "serial");

		printf ("syspath: %s\nsysname: %s\nsysnum: %s\ndevpath: %s\ndevnode: %s\n"
			"devtype: %s\nsubsystem: %s\ndriver: %s\nvid: %s\npid: %s\n"
			"manufacturer: %s\nproduct: %s\nserial: %s\n\n",
			syspath, sysname, sysnum, devpath, devnode,
			devtype, subsystem, driver, idvendor, idproduct,
			manufacturer, product, serial);

		udev_device_unref (udev_device);
	}

	udev_enumerate_unref (udev_enumerate);

	struct udev_monitor *udev_monitor;
	
	udev_monitor = udev_monitor_new_from_netlink (udev, "udev");

	if (!udev_monitor)
	{
		perror ("udev_monitor_new_from_netlink failure");
		udev_unref (udev);
		return errno;
	}

	if (udev_monitor_filter_add_match_subsystem_devtype (udev_monitor, "usb", NULL) < 0)
	{
		perror ("udev_monitor_filter_add_match_subsystem_devtype failure");
		udev_monitor_unref (udev_monitor);
		udev_unref (udev);
		return errno;
	}

	if (udev_monitor_enable_receiving (udev_monitor) < 0)
	{
		perror ("udev_monitor_enable_receiving failure");
		udev_monitor_unref (udev_monitor);
		udev_unref (udev);
		return errno;
	}

	int fd = udev_monitor_get_fd (udev_monitor);

	if (fd < 0)
	{
		perror ("udev_monitor_get_fd failure");
	}

	while (1)
	{
		fd_set fds;
		FD_ZERO (&fds);
		FD_SET (fd, &fds);

		int ret = select (fd + 1, &fds, NULL, NULL, NULL);

		if (ret <= 0)
			break;

		if (!FD_ISSET (fd, &fds))
		{
			continue;
		}
			
		struct udev_device *udev_device;

		udev_device = udev_monitor_receive_device (udev_monitor);

		if (!udev_device)
		{
			perror ("udev_monitor_receive_device failure");
			continue;
		}
		
		const char *syspath, *sysname, *sysnum, *devpath, *devnode,
		      *devtype, *subsystem, *driver, *idvendor, *idproduct,
		      *manufacturer, *product, *serial, *action;

		syspath = udev_device_get_syspath (udev_device);
		sysname = udev_device_get_sysname (udev_device);
		sysnum = udev_device_get_sysnum (udev_device);
		devpath = udev_device_get_devpath (udev_device);
		devnode = udev_device_get_devnode (udev_device);
		devtype = udev_device_get_devtype (udev_device);
		subsystem = udev_device_get_subsystem (udev_device);
		driver = udev_device_get_driver (udev_device);

		struct udev_device *parent = udev_device_get_parent_with_subsystem_devtype (
				udev_device, "usb", "usb_device");

		if (!parent)
		{
			perror ("udev_device_get_parent_with_subsystem_devtype failure");
			udev_device_unref (udev_device);
			continue;
		}

		idvendor = udev_device_get_sysattr_value (parent, "idVendor");
		idproduct = udev_device_get_sysattr_value (parent, "idProduct");
		manufacturer = udev_device_get_sysattr_value (parent, "manufacturer");
		product = udev_device_get_sysattr_value (parent, "product");
		serial = udev_device_get_sysattr_value (parent, "serial");

		action = udev_device_get_action (udev_device);

		printf ("syspath: %s\nsysname: %s\nsysnum: %s\ndevpath: %s\ndevnode: %s\n"
			"devtype: %s\nsubsystem: %s\ndriver: %s\nvid: %s\npid: %s\n"
			"manufacturer: %s\nproduct: %s\nserial: %s\n\naction: %s\n\n",
			syspath, sysname, sysnum, devpath, devnode,
			devtype, subsystem, driver, idvendor, idproduct,
			manufacturer, product, serial, action);

		udev_device_unref (udev_device);
	}

	udev_monitor_unref (udev_monitor);
	udev_unref (udev);

	return 0;
}
