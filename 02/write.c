#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

int main ()
{
	int fd;
	fd = open ("/dev/ttyUSB0", O_RDWR);
	if (fd == -1)
	{
		perror ("Unable to open /dev/ttyS0 - ");
		return 1;
	}

	char buf_out = 'X';
	if (write (fd, &buf_out, 1) != 1)
	{
		printf ("Write failed!\n");
		close (fd);
		return 2;
	}

	close (fd);
	return 0;
}
