#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

struct termios term;

int main ()
{
	int fd;
	fd = open ("/dev/ttyUSB0", O_RDWR);
	if (fd == -1)
	{
		perror ("Unable to open /dev/ttyS0 - ");
		return 1;
	}

	char buf_out[] = "Hello";
	if (write (fd, buf_out, 5) != 5)
	{
		printf ("Write failed!\n");
		close (fd);
		return 2;
	}

	char buf_in[6];
	memset (buf_in, '\0', sizeof(buf_in));

	if (read (fd, buf_in, 5) != 5)
	{
		printf ("Read failed!\n");
		close (fd);
		return 3;
	}

	printf ("%s\n", buf_in);
	close (fd);
	return 0;
}
