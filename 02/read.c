#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

int main ()
{
	int fd;
	fd = open ("/dev/ttyUSB0", O_RDWR);
	if (fd == -1)
	{
		perror ("Unable to open /dev/ttyS0 - ");
		return 1;
	}

	char buf_in;
	if (read (fd, &buf_in, 1) != 1)
	{
		printf ("Read failed!\n");
		close (fd);
		return 2;
	}

	printf ("%c\n", buf_in);
	close (fd);
	return 0;
}
