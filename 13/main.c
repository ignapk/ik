#include <stdio.h>
#include <errno.h>
#include <signal.h>
#include <libusb-1.0/libusb.h>

typedef struct libusb_device_descriptor libusb_device_descriptor;
typedef struct libusb_config_descriptor libusb_config_descriptor;
typedef struct libusb_interface_descriptor libusb_interface_descriptor;
typedef struct libusb_interface libusb_interface;
typedef struct libusb_endpoint_descriptor libusb_endpoint_descriptor;

int keep_running = 1;

void sigint_handler (int signum)
{
	keep_running = 0;
}

int print_endpoints (const libusb_interface_descriptor *des)
{
	for (uint8_t i = 0; i < des->bNumEndpoints; i++)
	{
		const libusb_endpoint_descriptor *endpoint = des->endpoint + i;

		printf ("Endpoint %u:\n"
			"endpointAddress=%02xh\nmAttributes=%02xh\n"
			"maxPacketSize=%u\ninterval=%u\n"
			"refresh=%u\nsynchAddress=%u\n\n",
			i,
			endpoint->bEndpointAddress, endpoint->bmAttributes,
			endpoint->wMaxPacketSize, endpoint->bInterval,
			endpoint->bRefresh, endpoint->bSynchAddress);
	}

	return 0;
}

int print_altsettings (libusb_device *device,
		       libusb_device_handle *handle,
		       const libusb_interface *interface)
{
	for (uint8_t i = 0; i < interface->num_altsetting; i++)
	{
		const libusb_interface_descriptor *desc =
			interface->altsetting + i;

		unsigned char interface[256];
		
		if (libusb_get_string_descriptor_ascii (handle,
							desc->iInterface,
							interface,
							256) < 0)
		{
			perror ("libusb_get_string_descriptor_ascii failure");
			interface[0] = '\0';
		}
		
		printf ("AltSetting %u:\ninterfaceNumber=%u\n"
			"alternateSetting=%u\nnumEndpoints=%u\n"
			"interfaceClass=%u\ninterfaceSubClass=%u\n"
			"interfaceProtocol=%u\ninterface=%s\n\n",
			i, desc->bInterfaceNumber,
			desc->bAlternateSetting, desc->bNumEndpoints,
			desc->bInterfaceClass, desc->bInterfaceSubClass,
			desc->bInterfaceProtocol, interface);

		if (print_endpoints (desc) != 0)
		{
			perror ("print_endpoints failure");
			return -1;
		}
	}

	return 0;
}

int print_interfaces (libusb_device *device,
		      libusb_device_handle *handle,
		      libusb_config_descriptor *config)
{
	for (uint8_t i = 0; i < config->bNumInterfaces; i++)
	{
		const libusb_interface *interface = config->interface + i;

		printf ("Interface %u:\nnumAltSetting=%d\n\n",
			i, interface->num_altsetting);

		if (print_altsettings (device, handle, interface) != 0)
		{
			perror ("print_altsettings failure");
			return -1;
		}
	}

	return 0;
}

int print_configurations (libusb_device *device,
			  libusb_device_handle *handle,
			  libusb_device_descriptor desc)
{
	for (uint8_t i = 0; i < desc.bNumConfigurations; i++)
	{
		libusb_config_descriptor *config;

		if (libusb_get_config_descriptor (device, i, &config) < 0)
		{
			perror ("libusb_get_config_descriptor failure");
			return -1;
		}

		unsigned char configuration[256];

		if (libusb_get_string_descriptor_ascii (handle,
							config->iConfiguration,
							configuration,
							256) < 0)
		{
			perror ("libusb_get_string_descriptor_ascii failure");
			configuration[0] = '\0';
		}

		printf ("Configuration %u:\nnumInterfaces=%u\n"
			"configurationValue=%u\nconfiguration=%s\n"
			"mAttributes=%u\nmaxPower=%u\n\n",
			i, config->bNumInterfaces,
			config->bConfigurationValue, configuration,
			config->bmAttributes, config->MaxPower);

		if (print_interfaces (device, handle, config) != 0)
		{
			perror ("print_interfaces failure");
			libusb_free_config_descriptor (config);
			return -2;
		}

		libusb_free_config_descriptor (config);
	}

	return 0;
}

int print_devices ()
{
	libusb_device **device_list;

	ssize_t count = libusb_get_device_list (NULL, &device_list);

	if (count < 0)
	{
		perror ("libusb_get_device_list failure");
		return errno;
	}

	for (ssize_t i = 0; i < count; i++)
	{
		libusb_device *device;

		device = device_list[i];

		uint8_t bus_number, port_number, device_address;
		int device_speed;
		const char *speed;

		bus_number = libusb_get_bus_number (device);
		port_number = libusb_get_port_number (device);
		device_address = libusb_get_device_address (device);
		device_speed = libusb_get_device_speed (device);

		switch (device_speed)
		{
			case LIBUSB_SPEED_LOW:		speed = "1.5M"; break;
			case LIBUSB_SPEED_FULL:		speed = "12M"; break;
			case LIBUSB_SPEED_HIGH:		speed = "480M"; break;
			case LIBUSB_SPEED_SUPER:	speed = "5G"; break;
			case LIBUSB_SPEED_SUPER_PLUS:	speed = "10G"; break;
			default:			speed = "?"; break;
		}

		printf ("bus_number=%u\nport_number=%u\n"
			"device_address=%u\ndevice_speed=%s\n",
			bus_number, port_number,
			device_address, speed);

		libusb_device_descriptor desc;

		if (libusb_get_device_descriptor (device, &desc) != 0)
		{
			perror ("libusb_get_device_descriptor failure");
			libusb_free_device_list (device_list, 1);
			return errno;
		}

		libusb_device_handle *device_handle;

		if (libusb_open (device, &device_handle) != 0)
		{
			perror ("libusb_open failure");
			libusb_free_device_list (device_list, 1);
			return errno;
		}


		unsigned char manufacturer[256], product[256], serialNum[256];

		if (libusb_get_string_descriptor_ascii (device_handle,
							desc.iManufacturer,
							manufacturer,
							256) < 0)
		{
			perror ("libusb_get_string_descriptor_ascii failure");
			/*libusb_free_device_list (device_list, 1);
			libusb_exit (NULL);
			return errno;*/
			manufacturer[0] = '\0';
		}

		if (libusb_get_string_descriptor_ascii (device_handle,
							desc.iProduct,
							product,
							256) < 0)
		{
			perror ("libusb_get_string_descriptor_ascii failure");
			/*libusb_free_device_list (device_list, 1);
			libusb_exit (NULL);
			return errno;*/
			product[0] = '\0';
		}

		if (libusb_get_string_descriptor_ascii (device_handle,
							desc.iSerialNumber,
							serialNum,
							256) < 0)
		{
			perror ("libusb_get_string_descriptor_ascii failure");
			/*libusb_free_device_list (device_list, 1);
			libusb_exit (NULL);
			return errno;*/
			serialNum[0] = '\0';
		}

		printf ("bcdUSB=%x\ndeviceClass=%u\ndeviceSubClass=%u\n"
			"deviceProtocol=%u\nidVendor=%04X\nidProduct=%04X\n"
			"manufacturer=%s\nproduct=%s\nserialNumber=%s\n"
			"numConfigurations=%u\n\n",
			desc.bcdUSB, desc.bDeviceClass, desc.bDeviceSubClass,
			desc.bDeviceProtocol, desc.idVendor, desc.idProduct,
			manufacturer, product, serialNum,
			desc.bNumConfigurations);

		libusb_close (device_handle);
	}

	libusb_free_device_list (device_list, 1);

	return 0;
}

int main ()
{
	if (libusb_init (NULL) != 0)
	{
		perror ("libusb_init failure");
		return errno;
	}

	if (print_devices () != 0)
	{
		perror ("print_devices failure");
		libusb_exit (NULL);
		return -1;
	}

	printf ("Enter device VID and PID separated with semicolon:\n");

	int idVendor, idProduct;
	scanf ("%x:%x", &idVendor, &idProduct);

	libusb_device_handle *handle;
	handle = libusb_open_device_with_vid_pid (NULL, idVendor, idProduct);

	if (handle == NULL)
	{
		perror ("libusb_open_device_with_vid_pid failure");
		libusb_exit (NULL);
		return errno;
	}

	libusb_device *device = libusb_get_device (handle);

	libusb_device_descriptor desc;

	if (libusb_get_device_descriptor (device, &desc) != 0)
	{
		perror ("libusb_get_device_descriptor failure");
		libusb_close (handle);
		libusb_exit (NULL);
		return errno;
	}

	if (print_configurations (device, handle, desc) != 0)
	{
		perror ("print_configurations failure");
		libusb_close (handle);
		libusb_exit (NULL);
		return errno;
	}

	printf ("Enter interface number and endpoint address +h separated with semicolon:\n");

	int interface_number, endpoint;
	scanf ("%d:%xh", &interface_number, &endpoint);
	
	if (libusb_detach_kernel_driver (handle, interface_number) != 0)
	{
		perror ("libusb_detach_kernel_driver failure");
		//libusb_close (handle);
		//libusb_exit (NULL);
		//return errno;
	}

	if (libusb_claim_interface (handle, interface_number) != 0)
	{
		perror ("libusb_claim_interface failure");
		libusb_close (handle);
		libusb_exit (NULL);
		return errno;
	}

	unsigned char data[8];
	int transferred, ret;

	signal (SIGINT, sigint_handler);

	while (ret = libusb_bulk_transfer (handle, endpoint, data,
					   sizeof (data), &transferred, 5000) == 0)
	{

		printf ("Read %d bytes from the endpoint:\n", transferred);
		for (int i = 0; i < transferred; i++)
			printf ("%08b ", data[i]);
		printf ("\n");

		if (!keep_running)
			break;
	}

	if (libusb_release_interface (handle, interface_number) != 0)
	{
		perror ("libusb_release_interface failure");
		libusb_close (handle);
		libusb_exit (NULL);
		return errno;
	}

	libusb_close (handle);
	libusb_exit (NULL);
	printf ("Exit\n");
	return 0;
}
