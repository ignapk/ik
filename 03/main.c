#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

int main ()
{
	int serial_port = open ("/dev/ttyUSB0", O_RDWR);

	if (serial_port < 0)
	{
		perror ("Error from open");
		return errno;
	}

	struct termios tty;

	if (tcgetattr (serial_port, &tty) != 0)
	{
		perror ("Error from tcgetattr");
		close (serial_port);
		return errno;
	}

	tty.c_cflag &= ~PARENB;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS8;
        tty.c_cflag &= ~CRTSCTS;
        tty.c_cflag |= CREAD | CLOCAL;

	tty.c_lflag &= ~ICANON;
        tty.c_lflag &= ~ECHO;
        tty.c_lflag &= ~ECHOE;
        tty.c_lflag &= ~ECHONL;
        tty.c_lflag &= ~ISIG;

	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
        tty.c_oflag &= ~OPOST;
        tty.c_oflag &= ~ONLCR;

	tty.c_cc[VTIME] = 1;
        tty.c_cc[VMIN] = 0;

	cfsetspeed (&tty, B9600);

	if (tcflush (serial_port, TCIOFLUSH) != 0)
	{
		perror ("Error from tcflush");
		close (serial_port);
		return errno;
	}

	if (tcsetattr (serial_port, TCSANOW, &tty) != 0)
	{
		perror ("Error from tcsetattr");
		close (serial_port);
		return errno;
	}

	int source = open ("src.txt", O_RDONLY);

	if (source < 0)
	{
		perror ("Error from open");
		close (serial_port);
		return errno;
	}

	char buf_out[2048];

	memset (buf_out, '\0', sizeof (buf_out));

	if (read (source, buf_out, sizeof (buf_out)) < 0)
	{
		perror ("Error from read");
		close (serial_port);
		close (source);
		return errno;
	}

	close (source);

	for (int i = 0, j = 1, end = 64; j; i += 64)
        {
                if (i > sizeof (buf_out))
                {
                        end = i - sizeof (buf_out);
                        j = 0;
                }
                int n = write (serial_port, buf_out + i, end);
                if (n < 0)
                {
                        perror ("Error from write");
                        close (serial_port);
                        return errno;
                }
        }

	char buf_in[2048];
	memset (buf_in, '\0', sizeof (buf_in));
	int n, i = 0;
        while ((n = read (serial_port, buf_in + i, 32)) > 0)
        {
                i += 32;
        }

        if(n < 0)
        {
                perror ("Error from read");
                close (serial_port);
                return errno;
        }

	int dest = open ("cel.txt", O_CREAT | O_WRONLY);

	if (dest < 0)
	{
		perror ("Error from open");
		close (serial_port);
		return errno;
	}

	if (write (dest, buf_in, sizeof (buf_in)) < 0)
	{
		perror ("Error from write");
		close (dest);
		close (serial_port);
		return errno;
	}

	close (dest);

	close (serial_port);
	return 0;
}
