#include <stdio.h>
#include <errno.h>
#include <libusb-1.0/libusb.h>

int main ()
{
	if (libusb_init (NULL) != 0)
	{
		perror ("libusb_init failure");
		return errno;
	}

	libusb_device **device_list;

	ssize_t count = libusb_get_device_list (NULL, &device_list);

	if (count < 0)
	{
		perror ("libusb_get_device_list failure");
		libusb_exit (NULL);
		return errno;
	}

	for (ssize_t i = 0; i < count; i++)
	{
		libusb_device *device;

		device = device_list[i];	

		uint8_t bus_number, port_number, device_address;
		int device_speed;

		bus_number = libusb_get_bus_number (device);
		port_number = libusb_get_port_number (device);
		device_address = libusb_get_device_address (device);
		device_speed = libusb_get_device_speed (device);

		printf ("bus_number=%u\nport_number=%u\n"
			"device_address=%u\ndevice_speed=%d\n\n",
			bus_number, port_number,
			device_address, device_speed);

		libusb_device_handle *device_handle;

		if (libusb_open (device, &device_handle) != 0)
		{
			perror ("libusb_open failure");
			libusb_free_device_list (device_list, 1);
			libusb_exit (NULL);
			return errno;
		}


		libusb_close (device_handle);
	}

	libusb_free_device_list (device_list, 1);
	libusb_exit (NULL);
	return 0;
}
