#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>

struct termios term;

int get_baud (speed_t baud);

int main ()
{
	int fd;
	fd = open ("/dev/ttyUSB0", O_RDWR | O_NOCTTY | O_NDELAY);
	if (fd == -1)
	{
		perror ("Unable to open /dev/ttyS0 - ");
	}
	else
	{
		if (tcgetattr (fd, &term) != 0)
		{
			perror ("Error from tcgetattr\n");
		}
		else
		{
			printf ("Reading serial port configuration:\n");
			printf ("Baud rate: %d\n", get_baud (cfgetospeed (&term)));
			printf ("Data bits: %d\n", (term.c_cflag & CSIZE) >> CSIZE);
			printf ("Stop bits: %d\n", (term.c_cflag & CSTOPB) ? 2 : 1);
			printf ("Parity: %s\n", (term.c_cflag &PARENB) ? "Odd" : "Even");
			
			printf ("Setting serial port configuration:\n");
			cfsetispeed(&term, B1200);
			printf ("c_cflag: set num stop bit\n");
			term.c_cflag |= CSTOPB;
			printf ("c_cflag: set parity bit\n");
			term.c_cflag |= PARENB;

			if (tcsetattr (fd, TCSANOW, &term) != 0)
			{
				perror ("Error from tcsetattr\n");
			}
		}
		close (fd);
	}
	return 0;
}


int get_baud (speed_t baud)
{
		switch (baud) {
    case B1200:
	    return 1200;
    case B9600:
        return 9600;
    case B19200:
        return 19200;
    case B38400:
        return 38400;
    case B57600:
        return 57600;
    case B115200:
        return 115200;
    case B230400:
        return 230400;
    case B460800:
        return 460800;
    case B500000:
        return 500000;
    case B576000:
        return 576000;
    case B921600:
        return 921600;
    case B1000000:
        return 1000000;
    case B1152000:
        return 1152000;
    case B1500000:
        return 1500000;
    case B2000000:
        return 2000000;
    case B2500000:
        return 2500000;
    case B3000000:
        return 3000000;
    case B3500000:
        return 3500000;
    case B4000000:
        return 4000000;
    default:
        return -1;
    }
}
