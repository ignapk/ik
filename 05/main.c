#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>

int main ()
{
	int serial_port = open ("/dev/ttyUSB0", O_RDWR);

	if (serial_port < 0)
	{
		perror ("Error from open");
		return errno;
	}

	struct termios tty;

	if (tcgetattr (serial_port, &tty) != 0)
	{
		perror ("Error from tcgetattr");
		close (serial_port);
		return errno;
	}

	tty.c_cflag &= ~PARENB;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS8;
        tty.c_cflag &= ~CRTSCTS;
        tty.c_cflag |= CREAD | CLOCAL;

	tty.c_lflag &= ~ICANON;
        tty.c_lflag &= ~ECHO;
        tty.c_lflag &= ~ECHOE;
        tty.c_lflag &= ~ECHONL;
        tty.c_lflag &= ~ISIG;

	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
        tty.c_oflag &= ~OPOST;
        tty.c_oflag &= ~ONLCR;

	tty.c_cc[VTIME] = 50;
        tty.c_cc[VMIN] = 0;

	//cfsetspeed (&tty, B9600);
	cfsetspeed (&tty, B460800);

	if (tcflush (serial_port, TCIOFLUSH) != 0)
	{
		perror ("Error from tcflush");
		close (serial_port);
		return errno;
	}

	if (tcsetattr (serial_port, TCSANOW, &tty) != 0)
	{
		perror ("Error from tcsetattr");
		close (serial_port);
		return errno;
	}

	int source = open ("hotdog.png", O_RDONLY);

	if (source < 0)
	{
		perror ("Error from open");
		close (serial_port);
		return errno;
	}

	char buf_out[1058130];

	int n;

	if ((n = read (source, buf_out, sizeof (buf_out))) < 0)
	{
		perror ("Error from read");
		close (serial_port);
		close (source);
		return errno;
	}

	printf ("Successfully read %d bytes from file\n", n);

	close (source);

	printf ("Press ENTER to continue\n");
	scanf ("%c");

	int i = 0;
	while (i < sizeof (buf_out))
	{
		printf ("Position %d\n", i);

		if (sizeof (buf_out) - i <= 64)
		{
			n = write (serial_port, buf_out + i, sizeof (buf_out) - i);
			printf ("Wrote %d to serial\n", n);
			break;
		}
		
		n = write (serial_port, buf_out + i, 64);
		printf ("Wrote %d to serial\n", n);
		i += 64;
	}

	printf ("Press ENTER to continue\n");
	scanf ("%c");

	char buf_in[1058130];

	i = 0;

	while (i < sizeof (buf_in))
	{
		printf ("Position %d\n", i);

		if (sizeof (buf_in) - i <= 32)
		{
			n = read (serial_port, buf_in + i, sizeof (buf_in) - i);
			printf ("Read %d from serial\n", n);
			break;
		}

		n = read (serial_port, buf_in + i, 32);

		if (n == 0)
		{
			perror ("Read from serial timeout exceeded");
			close (serial_port);
			return errno;
		}

		printf ("Read %d from serial\n", n);
		i += 32;
	}

	int dest = open ("colddog.png", O_CREAT | O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

	if (dest < 0)
	{
		perror ("Error from open");
		close (serial_port);
		return errno;
	}

	if ((n = write (dest, buf_in, sizeof (buf_in))) < 0)
	{
		perror ("Error from write");
		close (dest);
		close (serial_port);
		return errno;
	}

	printf ("Successfully written %d bytes to file\n", n);

	close (dest);

	close (serial_port);
	return 0;
}
