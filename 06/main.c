#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <termios.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

int main (int argc, char **argv)
{
	if (argc != 2)
	{
		printf ("Usage: %s <filename>\n", *argv);
		return errno;
	}

	int serial_port = open ("/dev/ttyUSB0", O_RDWR);

	if (serial_port < 0)
	{
		perror ("Error from open");
		return errno;
	}

	struct termios tty;

	if (tcgetattr (serial_port, &tty) != 0)
	{
		perror ("Error from tcgetattr");
		close (serial_port);
		return errno;
	}

	tty.c_cflag &= ~PARENB;
        tty.c_cflag &= ~CSTOPB;
        tty.c_cflag &= ~CSIZE;
        tty.c_cflag |= CS8;
        tty.c_cflag &= ~CRTSCTS;
        tty.c_cflag |= CREAD | CLOCAL;

	tty.c_lflag &= ~ICANON;
        tty.c_lflag &= ~ECHO;
        tty.c_lflag &= ~ECHOE;
        tty.c_lflag &= ~ECHONL;
        tty.c_lflag &= ~ISIG;

	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
        tty.c_iflag &= ~(IGNBRK|BRKINT|PARMRK|ISTRIP|INLCR|IGNCR|ICRNL);
        tty.c_oflag &= ~OPOST;
        tty.c_oflag &= ~ONLCR;

	tty.c_cc[VTIME] = 50;
        tty.c_cc[VMIN] = 0;

	//cfsetspeed (&tty, B9600);
	cfsetspeed (&tty, B460800);
	//cfsetspeed (&tty, B230400);

	if (tcflush (serial_port, TCIOFLUSH) != 0)
	{
		perror ("Error from tcflush");
		close (serial_port);
		return errno;
	}

	if (tcsetattr (serial_port, TCSANOW, &tty) != 0)
	{
		perror ("Error from tcsetattr");
		close (serial_port);
		return errno;
	}

	int source = open (*(argv + 1), O_RDONLY);

	if (source < 0)
	{
		perror ("Error from open");
		close (serial_port);
		return errno;
	}

	struct stat buf;
	fstat (source, &buf);
	printf ("%d\n", buf.st_size);
	char *buf_out = malloc (sizeof (char) * buf.st_size);

	int n;

	if ((n = read (source, buf_out, buf.st_size)) < 0)
	{
		perror ("Error from read");
		free (buf_out);
		close (serial_port);
		close (source);
		return errno;
	}

	printf ("Successfully read %d bytes from file\n", n);

	close (source);

	//printf ("Press ENTER to continue\n");
	//scanf ("%c");

	int i = 0;
	while (i < buf.st_size)
	{
		//printf ("Position %d\n", i);

		if (buf.st_size - i <= 64)
		{
			n = write (serial_port, buf_out + i, buf.st_size - i);
			if (n <= 0)
			{
				perror ("Error from write to serial");
				free (buf_out);
				close (serial_port);
				return errno;
			}
			//printf ("Wrote %d to serial\n", n);
			break;
		}
		
		n = write (serial_port, buf_out + i, 64);
		if (n <= 0)
		{
			perror ("Error from write to serial");
			free (buf_out);
			close (serial_port);
			return errno;
		}
		//printf ("Wrote %d to serial\n", n);
		i += 64;
	}

	free (buf_out);

	//printf ("Press ENTER to continue\n");
	//scanf ("%c");

	char *buf_in = malloc (sizeof (char) * buf.st_size);

	i = 0;

	while (i < buf.st_size)
	{
		//printf ("Position %d\n", i);

		if (buf.st_size - i <= 32)
		{
			n = read (serial_port, buf_in + i, buf.st_size - i);
			//printf ("Read %d from serial\n", n);
			break;
		}

		n = read (serial_port, buf_in + i, 32);

		if (n == 0)
		{
			perror ("Read from serial timeout exceeded");
			free (buf_in);
			close (serial_port);
			return errno;
		}

		//printf ("Read %d from serial\n", n);
		i += 32;
	}

	char prefix[] = "new";
	int dest = open (strcat (prefix, *(argv + 1)), O_CREAT | O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

	if (dest < 0)
	{
		perror ("Error from open");
		free (buf_in);
		close (serial_port);
		return errno;
	}

	if ((n = write (dest, buf_in, buf.st_size)) < 0)
	{
		perror ("Error from write");
		free (buf_in);
		close (dest);
		close (serial_port);
		return errno;
	}

	free (buf_in);

	printf ("Successfully written %d bytes to file\n", n);

	close (dest);

	close (serial_port);
	return 0;
}
